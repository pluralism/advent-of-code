const fs = require('fs');
const path = require('path');

const data = fs.readFileSync(path.join(process.cwd(), 'input.txt')).toString();
const numbers = data.split(/\r\n/).map(line => Number(line));
console.log(numbers.reduce((acc, v) => acc + v, 0));