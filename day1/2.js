const fs = require('fs');
const path = require('path');

function firstFrequency(list) {
    let total = 0;
    const set = new Set();

    set.add(total);

    while(true) {
        for(let value of list) {
            total += value;
            if(set.has(total)) {
                return total;
            }
            set.add(total);
        }
    }
}

const data = fs.readFileSync(path.join(process.cwd(), 'input.txt')).toString();
const numbers = data.split(/\r\n/).map(line => Number(line));
console.log(firstFrequency(numbers));